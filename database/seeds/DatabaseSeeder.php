<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$i=0;
    	while($i<5)
    	{
		    DB::table('blogs')->insert([
		    	'user_id'=>1,
		    	'title'=>"Lorem Ipsum is simply dummy text",
		    	'content'=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
		    ]);
		    $i++;    		
    	}

    }
}
