-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 17, 2020 at 08:48 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

DROP TABLE IF EXISTS `billings`;
CREATE TABLE IF NOT EXISTS `billings` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `institute_id` int(11) UNSIGNED NOT NULL,
  `billing_month` tinyint(2) NOT NULL,
  `billing_year` smallint(4) NOT NULL,
  `billing_amount` decimal(11,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `institute_id` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `desciption` text NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `create_status` enum('Draft','Complete') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
CREATE TABLE IF NOT EXISTS `features` (
  `id` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `institutes`
--

DROP TABLE IF EXISTS `institutes`;
CREATE TABLE IF NOT EXISTS `institutes` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_id` int(11) UNSIGNED NOT NULL,
  `institute_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `subscription_s_date` date NOT NULL,
  `subscription_e_date` date NOT NULL,
  `last_payment` date DEFAULT NULL,
  `payment_amount` decimal(11,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `institute_id` (`institute_id`),
  KEY `package_idForIns` (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institutes`
--

INSERT INTO `institutes` (`id`, `package_id`, `institute_id`, `password`, `name`, `address`, `email`, `phone`, `subscription_s_date`, `subscription_e_date`, `last_payment`, `payment_amount`, `created_at`, `updated_at`) VALUES
(1, 22, 'brac-int-100', '1234', 'Basic Package', 'Dhaka', 'ins@gmail.com', '38405893', '2020-07-19', '2020-07-19', '2020-03-21', '1000.00', '2020-08-16 11:27:48', '2020-08-16 11:27:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `institute_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `institute_id` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('4a14b0705d229b8779d461e4761ba73551217c50de838928e3370196d62c028e3312a49e79b3cec1', 2, 3, 'shewa12kpi@gmail.com', '[]', 0, '2020-08-05 03:37:20', '2020-08-05 03:37:20', '2020-08-19 09:37:16');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(20) UNSIGNED NOT NULL,
  `client_id` int(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(3, NULL, 'Laravel Personal Access Client', 'BzNKC4UigztNMtUGLFrC24cs0YMhIHuPr7IpTugL', NULL, 'http://localhost', 1, 0, 0, '2020-07-06 00:34:36', '2020-07-06 00:34:36'),
(4, NULL, 'Laravel Password Grant Client', 'WYbzjzCAwWf7BdSYCRVjjskfeYzUCoGVF4u9pDAL', 'users', 'http://localhost', 0, 1, 0, '2020-07-06 00:34:37', '2020-07-06 00:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(2, 3, '2020-07-06 00:34:37', '2020-07-06 00:34:37');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(16, 'Bangladesh University', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', '2020-08-09 07:27:15', '2020-08-09 07:27:15'),
(18, 'Brack University', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', '2020-08-09 07:42:03', '2020-08-09 07:42:03'),
(20, 'Green University', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', '2020-08-09 07:45:40', '2020-08-09 07:45:40'),
(22, 'Green University', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', '2020-08-16 11:27:47', '2020-08-16 11:27:47');

-- --------------------------------------------------------

--
-- Table structure for table `package_params`
--

DROP TABLE IF EXISTS `package_params`;
CREATE TABLE IF NOT EXISTS `package_params` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `packageIdForParams` (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package_params`
--

INSERT INTO `package_params` (`id`, `package_id`, `name`, `price`, `quantity`) VALUES
(5, 16, 'Total Students', '100.00', 30),
(6, 16, 'simultaneous Class', '1000.00', 10),
(7, 18, 'Total Students', '100.00', 30),
(8, 18, 'simultaneous Class', '1000.00', 10),
(9, 20, 'Total Students', '100.00', 30),
(10, 20, 'simultaneous Class', '1000.00', 10),
(11, 20, 'Streaming Service', '1000.00', 10),
(12, 20, 'Teacher', '2000.00', 10),
(17, 22, 'Total Students', '100.00', 30),
(18, 22, 'simultaneous Class', '1000.00', 10),
(19, 22, 'Streaming Service', '1000.00', 10),
(20, 22, 'Teacher', '2000.00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `package_param_junction`
--

DROP TABLE IF EXISTS `package_param_junction`;
CREATE TABLE IF NOT EXISTS `package_param_junction` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_id` int(11) UNSIGNED NOT NULL,
  `param_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `package_id` (`package_id`),
  KEY `param_id` (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('shewa12kpi@gmail.com', 'E1lxXEqI21BTq1qazUV99RHvqPzdLgxbMBiCL33Jl5yOzj6EyDalh5FxqvtKxGEqKO3dThpjHqY1cn21rSP7CyXLLlHVENR8iVQF', '2020-07-23 03:59:18'),
('shewa12kpi@gmail.com', 'xs1rAehNRNMpHtzPPo71fwglKR0LwUOvK403uTwFSs3HGirWMjEs0xGfFn3QZE3IQQ8gNEswOi2laQxb9kb1ClmDaWEVC28CONiO', '2020-07-23 04:05:31'),
('shewa12kpi@gmail.com', 'VPDeYXNaW0e4y6mJWUCmXZJHx4UtZwyVJyn2b26008rBCOtm7JOVeCfiajGkEoCOAhtC3PHy62WoKVWqrDb2bhXqwslAuy38nnX7', '2020-07-27 05:52:59'),
('shafaet3@gmail.com', '7Ua4nBwSZN542eveIm7ZYmBhjj5YPtPAgFcCVKH8Ddjb9BQdfd1lY1vvV6OlWPEZPQMwjGg1ZsAVU6ag4N1yANwPwbBoY7IerxX0', '2020-07-27 05:57:40'),
('shafaet3@gmail.com', 'KdqjmKKuPtwdBsmAc9FEzkn3Jx2ABWo8cMENHQv0Q0Lm6eD3jyXVOVxx8hdau3NhHL7PDAQswl7mXEbC6Fhp05DAPcG6sUYWCc0b', '2020-07-27 05:59:34'),
('shafaet3@gmail.com', 'P2UsvvattDAX7tmU01ITsuEUOJwK5c7JcCJSuoNldJcgQcySxdiC4Q2wN2si2SBzOsGqWq7VUpZeqzLpHtvTkvnFocdCPXZkZK0e', '2020-07-27 06:02:52'),
('shafaet3@gmail.com', 'a36T5UOkqS9K65xn1RiHfG3aSwmeixx2greUBfyxDNWA8Pin9ciXPCpyriOo0TnuILHQCQxcKoqyBdOJcGI4gBpPIKcjET6iHBMH', '2020-07-27 09:43:52'),
('sohag933@gmail.com', 'XySC3qoh1yjZP4whvwFEHLwovYvJ6NMdSmHMExkipVAZOh0SNWTlI9nA6zTX1J0dr3umx64HipCX7GBnq2oZhqqo3L2yDIf5QsgC', '2020-08-04 23:52:33'),
('shafaet3@gmail.com', 'hrdJdf837SjqbH5L5CRUFwct4PENmEMDSLzUi08jV2lyM9CqqLnkQ1vB9y6NWRFOhdZ71a9HQUpzjHATFsCN9TYaxMZzLjSvfASD', '2020-08-05 00:56:30');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `institute_id` int(11) UNSIGNED NOT NULL,
  `billing_id` int(11) UNSIGNED NOT NULL,
  `attachment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `billing_id` (`billing_id`),
  KEY `institute_id` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `role_scopes`
--

DROP TABLE IF EXISTS `role_scopes`;
CREATE TABLE IF NOT EXISTS `role_scopes` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` smallint(11) UNSIGNED NOT NULL,
  `feature_id` smallint(11) UNSIGNED NOT NULL,
  `scope` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `feature_id` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `to_cc_bcc`
--

DROP TABLE IF EXISTS `to_cc_bcc`;
CREATE TABLE IF NOT EXISTS `to_cc_bcc` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) UNSIGNED NOT NULL,
  `to_id` int(11) DEFAULT NULL,
  `cc_id` int(11) DEFAULT NULL,
  `bcc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mailId` (`mail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_status` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` smallint(11) UNSIGNED NOT NULL,
  `avatar_img` varchar(255) DEFAULT NULL,
  `cover_img` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(14) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `place_of_birth` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `nid` varchar(20) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `roleIdForUsers` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_status`, `role_id`, `avatar_img`, `cover_img`, `username`, `password`, `fname`, `lname`, `email`, `mobile`, `date_of_birth`, `place_of_birth`, `gender`, `marital_status`, `religion`, `nationality`, `nid`, `designation`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'images.jpg', '42253552-a-beautiful-waterfall-in-the-forest-on-a-river.jpg', 'shewa', '$2y$10$pvmIgw9EcueXgr/fcHOl8u7N37ekUOlElbgz9JLDV6BePIdAJBLxq', 'shewa', 'shewa', 'shewa12kpi@gmail.com', '01921990033', '1992-10-11', 'Khulna', 'Male', 'Unmarried', 'Dhaka', 'Bangladeshi', '12345564444444444', 'Admin', '2020-07-06 05:54:41', '2020-07-27 11:56:06'),
(3, 1, 1, 'avatar5.png', 'back.jpg', 'shafaet', '$2y$10$Bp08PuaeQmDvtmisVeTx7OpdsP0lZn9qHw.36GUydbRARCHFeiilq', 'Ninad', 'Hossain', 'shafaet3@gmail.com', '01234567899', '2020-07-28', 'Dhaka', 'Male', 'Single', 'Hinduism', 'Bangladeshi', '1231316549879874', 'Super Admin', '2020-07-23 22:10:28', '2020-08-05 06:57:46'),
(4, 1, 1, '2019-03-02-13-10-46-587.jpg', '20161126_161912.jpg', 'sohag', '$2y$12$XF.x2wrJAcjntfMHeH6vRePqS3tUEgO2VxJpWEJPB5QSB.CaIRDTO', 'Sohag', 'Ahmed', 'sohag933@gmail.com', '01717662707', '1990-08-31', 'Bogura', 'Male', 'Married', 'Islam', 'Bangladeshi', '0009956876', 'Software Engineer (QA)', '2020-07-27 17:43:37', '2020-07-30 12:02:48');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

DROP TABLE IF EXISTS `user_address`;
CREATE TABLE IF NOT EXISTS `user_address` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `address` varchar(255) NOT NULL,
  `post_office` varchar(255) DEFAULT NULL,
  `post_code` int(6) DEFAULT NULL,
  `thana` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `per_address` varchar(255) DEFAULT NULL,
  `per_post_office` varchar(255) DEFAULT NULL,
  `per_post_code` int(6) DEFAULT NULL,
  `per_thana` varchar(255) DEFAULT NULL,
  `per_district` varchar(255) DEFAULT NULL,
  `per_division` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `user_id`, `address`, `post_office`, `post_code`, `thana`, `district`, `division`, `per_address`, `per_post_office`, `per_post_code`, `per_thana`, `per_district`, `per_division`) VALUES
(2, 2, 'Khulna', 'shewa12', 1100, 'Dhaka', 'Dhaka', 'Dhaka', 'shewa18', 'shewa12', 1200, 'Dhaka', 'Dhaka', 'Dhaka'),
(3, 3, 'Lalmatiya', '2020', 2990, 'Dhaka', 'Dhaka', 'Dhaka', 'Dhanmondi', '123456', 2222, 'Dhaka', 'Dhaka', 'Dhaka'),
(4, 4, 'House no: 85/1/A, Road: 04, Mohammadia Housing Limited, Mohammadpur', 'Mohammadpur', 1207, 'Dhaka', 'Dhaka', 'Dhaka', 'House no: 85/1/A, Road: 04, Mohammadia Housing Limited, Mohammadpur', 'Mohammadpur', 1207, 'Dhaka', 'Dhaka', 'Dhaka');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `billings`
--
ALTER TABLE `billings`
  ADD CONSTRAINT `billings_ibfk_1` FOREIGN KEY (`institute_id`) REFERENCES `institutes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `institutes`
--
ALTER TABLE `institutes`
  ADD CONSTRAINT `package_idForIns` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`institute_id`) REFERENCES `institutes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package_params`
--
ALTER TABLE `package_params`
  ADD CONSTRAINT `packageIdForParams` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package_param_junction`
--
ALTER TABLE `package_param_junction`
  ADD CONSTRAINT `package_param_junction_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `package_param_junction_ibfk_2` FOREIGN KEY (`param_id`) REFERENCES `package_params` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`institute_id`) REFERENCES `institutes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `payments_ibfk_2` FOREIGN KEY (`billing_id`) REFERENCES `billings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_scopes`
--
ALTER TABLE `role_scopes`
  ADD CONSTRAINT `role_scopes_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_scopes_ibfk_2` FOREIGN KEY (`feature_id`) REFERENCES `features` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `to_cc_bcc`
--
ALTER TABLE `to_cc_bcc`
  ADD CONSTRAINT `mailId` FOREIGN KEY (`mail_id`) REFERENCES `emails` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `roleIdForUsers` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
  ADD CONSTRAINT `user_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
