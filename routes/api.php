<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return response()->json(['ok'=>'ok']);
});
*/
Route::get('/login',function(){

	$res = ['status'=>'failed','msg'=>"Invalid access token"];
	return response($res,401);
	
})->name('login');

//guest routes
Route::namespace('Api')->group(function(){

	Route::post('authenticate','AuthCtrl@authenticate');

	Route::post('password-reset-request','AuthCtrl@passwordResetRequest');	
	
	Route::post('password-reset','AuthCtrl@passwordReset');	

});
//guest routes end

//auth routes
//'auth:api'
Route::namespace('Api')->middleware('auth:api')->group(function()
//Route::namespace('Api')->group(function()

{

	//users basic api
	Route::get('/user-detail/{id}',"UserCtrl@userDetail")->where('id','[0-9]+');

	Route::post('/info-update',"UserCtrl@infoUpdate");

	Route::post('/avatar-update',"UserCtrl@avatarUpdate");

	Route::post('/cover-img-update',"UserCtrl@coverImgUpdate");
	
	Route::post('/password-update',"UserCtrl@updatePassword");

	Route::get('user-address/{userId}','AddressCtrl@userAddress')->where('userId','[0-9]+');

	Route::post('update-user-address','AddressCtrl@updateUserAddress');

	Route::get('/logout',"UserCtrl@logout");
	//users basic api

	//package parameter api
	Route::get('/all-packages',"PackageParamHandleCtrl@allPackages");

	Route::get('/package-detail/{id}',"PackageParamHandleCtrl@packageDetail")->where('id','[0-9]+');

	Route::post('/package-save',"PackageParamHandleCtrl@savePackage");

	Route::post('/package-update',"PackageParamHandleCtrl@updatePackage");

	Route::delete('/package-delete/{id}',"PackageParamHandleCtrl@deletePackage")->where('id','[0-9]+');

	Route::get('/parameters/{packageId}',"PackageParamHandleCtrl@getParameterByPackageId")->where('packageId','[0-9]+');

	Route::post('/parameter-save',"PackageParamHandleCtrl@saveParameter");

	Route::post('/parameter-update',"PackageParamHandleCtrl@updateParameter");

	Route::delete('/parameter-delete/{id}',"PackageParamHandleCtrl@deleteParameter")->where('id','[0-9]+');
	
	//package parameter api end

	//institute 
	Route::get('/institutes',"InstituteCtrl@institutes");

	Route::get('/institute-detail/{id}',"InstituteCtrl@instituteDetail")->where('id','[0-9]+');

	Route::post('/institute-create',"InstituteCtrl@saveInstitute");

	Route::post('/institute-update',"InstituteCtrl@updateInstitute");
	
	Route::post('/institute-status-update',"InstituteCtrl@updateInstituteStatus");

	Route::delete('/institute-delete/{id}',"InstituteCtrl@deleteInstitute")->where('id','[0-9]+');
	//institute end
});

Route::get('/test',function(){
	$mail = Mail::to('shewa12kpi@gmail.com')->send(new App\Mail\Test);	
	dd($mail);
});


