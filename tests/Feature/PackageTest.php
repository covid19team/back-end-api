<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Package;

class PackageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    function testPackageSave()
    {
        $post = ['title'=>'test','description'=>'test desc'];

        $response = $this->json('POST', '/api/package-save', $post);

        $response->assertStatus(200);
    }   

     function testPackageUpdate()
    {
        $post = ['title'=>'test','description'=>'test desc'];

        $response = $this->json('POST', '/api/package-save', $post);

        $response->assertStatus(200);
    }    

    function testPackageDelete()
    {
    
        $response = $this->delete('/api/package-delete/5');

        $response->assertStatus(200);
    }
}
