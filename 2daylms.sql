-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 05, 2020 at 08:11 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

DROP TABLE IF EXISTS `billings`;
CREATE TABLE IF NOT EXISTS `billings` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `institute_id` int(11) UNSIGNED NOT NULL,
  `billing_month` tinyint(2) NOT NULL,
  `billing_year` smallint(4) NOT NULL,
  `billing_amount` decimal(11,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `institute_id` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `desciption` text NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `create_status` enum('Draft','Complete') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

DROP TABLE IF EXISTS `features`;
CREATE TABLE IF NOT EXISTS `features` (
  `id` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `institutes`
--

DROP TABLE IF EXISTS `institutes`;
CREATE TABLE IF NOT EXISTS `institutes` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_id` int(11) UNSIGNED NOT NULL,
  `institute_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `subscription_s_date` date NOT NULL,
  `subscription_e_date` date NOT NULL,
  `last_payment` date DEFAULT NULL,
  `payment_amount` decimal(11,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `institute_id` (`institute_id`),
  KEY `packageIdForInst` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `institute_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `institute_id` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(20) UNSIGNED NOT NULL,
  `client_id` int(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(2, NULL, 'Learnerscafe Password Grant Client', 'mwE6Hl8Ioox1o0RAyQGVPT6TusBoPNZV6vVcv5Yh', 'users', 'http://localhost', 0, 1, 0, '2020-08-05 02:02:45', '2020-08-05 02:02:45'),
(5, NULL, 'Lumen Personal Access Client', 'm2Luw4zpHECSqy1M5EZ0HC1Ss2g2db6PQxpOSmGg', NULL, 'http://localhost', 1, 0, 0, '2020-07-28 08:17:24', '2020-07-28 08:17:24'),
(6, NULL, 'Lumen Password Grant Client', 'qlX2gymmVzkKa03jDjWqbb3bhHf12qLQXxWnaYa2', 'users', 'http://localhost', 0, 1, 0, '2020-07-28 08:17:25', '2020-07-28 08:17:25'),
(7, NULL, 'Learnerscafe Personal Access Client', 'yAcHx9WZevkX43Qiy1PcTcAqP69WP1YhoQLPa0fr', NULL, 'http://localhost', 1, 0, 0, '2020-08-05 02:02:45', '2020-08-05 02:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 5, NULL, NULL),
(4, 7, '2020-08-05 02:02:45', '2020-08-05 02:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `package_params`
--

DROP TABLE IF EXISTS `package_params`;
CREATE TABLE IF NOT EXISTS `package_params` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `package_param_junction`
--

DROP TABLE IF EXISTS `package_param_junction`;
CREATE TABLE IF NOT EXISTS `package_param_junction` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_id` int(11) UNSIGNED NOT NULL,
  `param_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `package_id` (`package_id`),
  KEY `param_id` (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('shewa12kpi@gmail.com', 'yTJPeHESD6ExzN3gkndBa05YELEmpHyuktf2ee7Z1QhVqqCaJSIduNAylWLZAuDHB5QMlBr0esMXhPHAFDpxmZvLRlMuDFRq3KHY', '2020-07-29 13:34:19'),
('shewa12kpi@gmail.com', 'SXOgcvRvhEvqQ4KDpeWOIlsUju2MrpKupkznJdZUi6Hnxj4Qalpit0qENxARYeo0Dg7WkCHK3rYmCRbASwqWe4YpGvtjtS1XgATB', '2020-07-29 13:44:34'),
('shewa12kpi@gmail.com', 'lsLsUnzslZWkZMGx0nmMMpJhu7hygyQUgHIci3cQMK2efW8GWPXUNK8v5QqWa7bOxDw76MI4rm0ldyIChT4Fk2JdyMvAyadotc6H', '2020-07-29 13:46:57'),
('shewa12kpi@gmail.com', '0F8334zxRRFoc0Os7s4YmTEcsyd3AuPIbDvqPg9PzU4bC9aOqP39YrthgTrbQcCev0ndyqBM4ysNTYBimI6qQ2cGqwKyLPrv0mcj', '2020-07-29 13:47:58'),
('shewa12kpi@gmail.com', 'uJy0lADKtdV9MNDwO4ICrFox4KWfS1s7z3fELcZwlOTc2UxKgkZ76a0CfRuS8f2DNEN3k7SqYkwL6c6iCCfo1VOgbtNWpvXAqFW0', '2020-07-29 13:48:58'),
('shewa12kpi@gmail.com', 'CVOvamW6O8qkmrRtD1oAXRlRBtr8nMIVkGKkCwYM6DFeSIDJwqV3FFV2FwEtPqSKsujOznfUO2sT4JnTC7EgunujCRrhA0WyI4mU', '2020-07-29 13:49:17'),
('shewa12kpi@gmail.com', 'qKv9XDnCVnejoi67dmM0JdkcX3vpj2oh8cEer58rcevXWEoSJtxqOthUSNSjMUBJXgiDSLC1SnPO96hr0tw7QPcrmWpMeBCXSzA6', '2020-07-29 13:51:20'),
('shewa12kpi@gmail.com', 'XZc3lvLoKAFVhGMl4ApWxrBlLU5Sn6139g4bD2hizSveMmHs7UDoSn8X4i0FKygsYNbf71LezkTczyCKWOUDHZDDS5rfktlBVOLY', '2020-07-29 13:52:28'),
('shewa12kpi@gmail.com', '4CIgwKoUrQsjWRXOpwo5xzvlJtjU71hIz7n0skVYYIMh1STeNqJBaXCXx0YqtS9gTqP0zPcVTDXVqECRQ0IzEvxNKkFHJUl6NIuo', '2020-07-29 13:53:31'),
('shewa12kpi@gmail.com', 'Wal43XCplQ0py9sDXeGZiks6ZYPAJc2MiPHU8hc6UvPGoKyCoTePF8PwUfHAVnOOVnXUUFez3GJl6amO309XSV8IURtnqVXRzHl7', '2020-07-29 14:01:07'),
('shewa12kpi@gmail.com', 'KFiuaFm4ipPhnHjHwg68W6PbDwkKXhxI1s98st1mL1P4WaWaWSXdiGvf364cr7sVw7xFKaxP3cO84igY6MQwDDsumrmfzjsZcHu5', '2020-07-29 14:03:38'),
('shewa12kpi@gmail.com', '3vdyr01xL7AVufY4amFU0nkDTNsOiwcFf1t1VdIIFBDo6VmdBb3iLHhpjgAMC5WUOvWSHGyYZoR1DBUTq7SQk6eg05jzU0y4tkza', '2020-07-29 14:07:27'),
('shewa12kpi@gmail.com', 'DLwU7qCMe8awxgcfJMQkqzLbvk22N9tfj6BE81G7dqt62YL9dA63nPyYWxqsSrRIyh3vnW1XrxZt4atHlNTH6j7FR2722PD6lVsT', '2020-07-29 14:08:15');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `institute_id` int(11) UNSIGNED NOT NULL,
  `billing_id` int(11) UNSIGNED NOT NULL,
  `attachment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `billing_id` (`billing_id`),
  KEY `institute_id` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `role_scopes`
--

DROP TABLE IF EXISTS `role_scopes`;
CREATE TABLE IF NOT EXISTS `role_scopes` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` smallint(11) UNSIGNED NOT NULL,
  `feature_id` smallint(11) UNSIGNED NOT NULL,
  `scope` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `feature_id` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `to_cc_bcc`
--

DROP TABLE IF EXISTS `to_cc_bcc`;
CREATE TABLE IF NOT EXISTS `to_cc_bcc` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) UNSIGNED NOT NULL,
  `to_id` int(11) DEFAULT NULL,
  `cc_id` int(11) DEFAULT NULL,
  `bcc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mailId` (`mail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_status` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` smallint(11) UNSIGNED NOT NULL,
  `avatar_img` varchar(255) DEFAULT NULL,
  `cover_img` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(14) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `place_of_birth` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `nid` varchar(20) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `roleIdForUsers` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_status`, `role_id`, `avatar_img`, `cover_img`, `username`, `password`, `fname`, `lname`, `email`, `mobile`, `date_of_birth`, `place_of_birth`, `gender`, `marital_status`, `religion`, `nationality`, `nid`, `designation`, `created_at`, `updated_at`) VALUES
(2, 1, 1, NULL, NULL, 'shewa', '$2b$10$Z5e1grxiR8JjUh0oeo4g7ezT4wkT.PSO5zi.aw.0LDR9PI.6ywyhu', 'Shewa', 'Ahmed', 'shewa12kpi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-28 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

DROP TABLE IF EXISTS `user_address`;
CREATE TABLE IF NOT EXISTS `user_address` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `address` varchar(255) NOT NULL,
  `post_office` varchar(255) DEFAULT NULL,
  `post_code` int(6) DEFAULT NULL,
  `thana` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `per_address` varchar(255) DEFAULT NULL,
  `per_post_office` varchar(255) DEFAULT NULL,
  `per_post_code` int(6) DEFAULT NULL,
  `per_thana` varchar(255) DEFAULT NULL,
  `per_district` varchar(255) DEFAULT NULL,
  `per_division` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `billings`
--
ALTER TABLE `billings`
  ADD CONSTRAINT `billings_ibfk_1` FOREIGN KEY (`institute_id`) REFERENCES `institutes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `institutes`
--
ALTER TABLE `institutes`
  ADD CONSTRAINT `packageIdForInst` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`institute_id`) REFERENCES `institutes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `package_param_junction`
--
ALTER TABLE `package_param_junction`
  ADD CONSTRAINT `package_param_junction_ibfk_1` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `package_param_junction_ibfk_2` FOREIGN KEY (`param_id`) REFERENCES `package_params` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`institute_id`) REFERENCES `institutes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `payments_ibfk_2` FOREIGN KEY (`billing_id`) REFERENCES `billings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_scopes`
--
ALTER TABLE `role_scopes`
  ADD CONSTRAINT `role_scopes_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_scopes_ibfk_2` FOREIGN KEY (`feature_id`) REFERENCES `features` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `to_cc_bcc`
--
ALTER TABLE `to_cc_bcc`
  ADD CONSTRAINT `mailId` FOREIGN KEY (`mail_id`) REFERENCES `emails` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `roleIdForUsers` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
  ADD CONSTRAINT `user_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
