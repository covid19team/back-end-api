<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = "user_address";
    public $timestamps = false; 
    public $guarded = [];
}
