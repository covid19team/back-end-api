<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\PackageCtrl;
use Illuminate\Http\Request;
use App\Institute;

class InstituteCtrl extends Controller implements CrudInterface
{
    use CrudModel;
    use ResponseTrait;
    use ResponseCodeTrait;
    const table ="institutes";
    const pack_table ="packages";
    const param_table ="package_params";
    const table_path = 'App\Institute';

    function institutes()
    {
        return $this->response($this->ok,$this->getInstiutes());
    }

    //institute list with pagination
    function getInstiutes():object
    {
        return Institute::paginate(20);
    }

    function saveInstitute(Request $request)
    {
        $package = new PackageCtrl;
        if(!$package->checkPackage($request->package_id))
        {
            return $this->response($this->failed,'Invalid package id');
        }
        $validate = $this->validateInstitute($request->all());

        if($validate->fails())
        {
            return $this->response($this->vError,$validate->errors());
        }

        $create = $this->create($request->all(),self::table_path);

        return $create ? $this->response($this->ok,'Institute created') : $this->response($this->failed,'Institute creation failed');
    }

    function updateInstituteStatus(Request $request)
    {
        $post = $request->all();
        $validate = $this->updateStatusValidation($post);
        if($validate->fails())
        {
            return $this->response($this->vError,$validate->errors());
        }

        return $this->update($post,self::table_path) ? $this->response($this->ok,"Institute status udpated") : $this->response($this->failed,"Institute status udpate failed"); 
    }

    function updateInstitute(Request $request)
    {
        $post = $request->all();

        $validate = $this->validateInstituteUpdate($post);

        if($validate->fails())
        {
            return $this->response($this->vError,$validate->errors());
        }
        unset($post['institute_id']);

        return $this->update($post,self::table_path)? $this->response($this->ok,'Institute updated successfully') : $this->response($this->failed,'Institute update failed');
    }

    function deleteInstitute(int $id)
    {
        $delete = $this->delete($id,self::table_path);

        return $delete ? $this->response($this->ok,'Institute deleted successfully') : $this->response($this->failed,'Institute delete failed or invalid id'); 
    }

    function instituteDetail(int $instituteId)
    {
        $institute = $this->instituteWithPackageParam($instituteId);
        return $institute ? $this->response($this->ok,$institute) : $this->response($this->failed,"Institute not found");
    }


    function instituteWithPackageParam(int $id)//object or null
    {
        return DB::table(self::table.' as i')
            ->select('i.*','p.title as package_title','p.description as package_desc','pp.name as param_name','pp.quantity as param_quantity','pp.price as param_price')
            ->join(self::pack_table.' as p','p.id','=','i.package_id')
            ->join(self::param_table.' as pp','pp.package_id','=','p.id')
            ->where('i.id',$id)
            ->first();
    }

    function validateInstitute($request)
    {
        
        return Validator::make($request,[
            
            "institute_id"=> "required|string|max:255|unique:institutes",
            "password"=> "required|min:6",
            "name"=> "required|string|max:255",
            "address"=>"required|string|max:255",
            "email"=>"required|email|max:255",
            "phone"=> "required|numeric",
            "subscription_s_date"=> "required|date_format:Y-m-d",
            "subscription_e_date"=> "required|date_format:Y-m-d",
            "last_payment"=> "required|date_format:Y-m-d",
            "payment_amount"=> "required|numeric",
        ]);     
    }     

    function validateInstituteUpdate($request)
    {
        
        return Validator::make($request,[
            "id"=>"required|numeric",
            "password"=> "required|min:6",
            "name"=> "required|string|max:255",
            "address"=>"required|string|max:255",
            "email"=>"required|email|max:255",
            "phone"=> "required|numeric",
            "subscription_s_date"=> "required|date_format:Y-m-d",
            "subscription_e_date"=> "required|date_format:Y-m-d",
            "last_payment"=> "required|date_format:Y-m-d",
            "payment_amount"=> "required|numeric",
            
        ]);     
    }

    function updateStatusValidation($request)
    {
        return Validator::make($request,[
            'id'=>'required|numeric',
            'active'=>'required|numeric',
        ]);
    }   
}
