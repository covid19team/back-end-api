<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UserCtrl;
use App\Notifications\PasswordReset;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

use App\User;
use Exception;
interface _Auth{
    /*
        *validating, authentication & create personal access token for a user
    */
    function authenticate(Request $request);

    /*
        *login validation
    */
    function validateLogin(array $request);
}

class AuthCtrl extends Controller implements _Auth
{
	use ResponseTrait;
    use ResponseCodeTrait;

    function authenticate(Request $request)
    {
        $validation = $this->validateLogin($request->all());

        if($validation->fails())
        {
            return $this->response('validationError',$validation->errors());
        }

        $credentials= [
                'email'=> $request->email,
                'password'=> $request->password,
                'user_status'=>1
            ];
            
        $authenticate = Auth::attempt($credentials);  

        if($authenticate)
        {
            $user = $request->user();
            $token = $user->createToken($request->email)->accessToken;
            $msg = [
                'id'=> $user->id,
                'token'=> $token
            ];
            return $this->response('ok',$msg);

        }
        else
        {
            return $this->response('Invalid credential',401);
        }

    }

    function passwordResetRequest(Request $request)
    {
        $token = Str::random('100');
        $email = $request->email;
        $exists = $this->emailExists($email);
        if(!$exists)
        {
            return $this->response($this->failed,'Email not exists');
        }

        //send notification
        if($this->insertPasswordReset($email,$token))
        {
        
           
            $notify = Notification::route('mail',$email)
                        ->notify(new PasswordReset($token));
                        
                
            return $this->response($this->ok,$token); 
                       
        }
        return $this->response($this->failed,'Password reset token insert failed');

    }

    function passwordReset(Request $request)
    {
        $validate = $this->passwordResetValidation($request->all());
        if($validate->fails())
        {
            return $this->response($this->vError,$validate->errors());
        }

        if($this->tokenValid($request->token))
        {
            $post = [
                'email'=> $request->email,
                'password'=> bcrypt($request->password)
            ];
            //update password
            $update = $this->updatePasswordByMail($post);

            if($update)
            {
                $this->deleteToken($request->email);
                return $this->response($this->ok,'Password reset success');
            } 
            $this->response($this->failed,'Password reset failed');
        }
        return $this->response($this->failed,'Password reset token is not valid');
    }

    function tokenValid(string $token) :bool
    {
        $find = DB::table('password_resets')->select('token')->where('token',$token)->first();

        return $find ? true : false;
    }    

    function emailExists(string $email) :bool
    {
        $find = DB::table('users')->select('id')->where('email',$email)->first();
        return $find ? true : false;
    }

    function updatePasswordByMail(array $post): bool
    {
        return DB::table('users')->where('email',$post['email'])
            ->update(['password'=>$post['password']]);
    }

    function insertPasswordReset(string $email,string $token) : bool
    {
        return DB::table('password_resets')->insert([
            'email'=> $email,
            'token'=> $token
        ]);        
    }    

    function deleteToken(string $email) : void
    {
        DB::table('password_resets')->where([
            'email'=> $email,
        ])->delete();        
    }

    function passwordResetValidation(array $request)
    {
        return Validator::make($request,[
                'token'=>'required',
                'email'=>'required',
                'password'=> 'required|between:6,50 ',
                'password_confirmation'=> 'same:password'
                
        ]);         
    }

    function validateLogin(array $requests)
    {
        return Validator::make($requests,[
            'email'=> 'required|email',
            'password'=> 'required'
        ]);
    }
  
}
