<?php 
namespace App\Http\Controllers\Api;

trait ResponseTrait {
	
	function response($status,$msg)
	{
		$res = ['status'=> $status,'msg'=> $msg];
		return response()->json($res);
	}
}
?>