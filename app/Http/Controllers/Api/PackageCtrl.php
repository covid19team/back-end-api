<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Package;

class PackageCtrl extends Controller
{
	use ResponseTrait;
	use ResponseCodeTrait;
 
   const package_table = 'packages';
   const param_table = 'package_params';

   //return object with package and param. pagination
   function packagesWithParam():object
   {
      
      return DB::table(self::package_table .' as p')
         ->select('p.*','param.id as param_id','param.name','param.quantity','param.price')
         ->leftJoin(self::param_table.' as param','p.id','=','param.package_id')
         ->paginate(10);

   }

   //require package id return obj package with param
   static function singlePackageDetail(int $packageId):object
   {
      $package = Package::find($packageId);
      $params = DB::table(self::package_table .' as p')
         ->select('param.id as param_id','param.name','param.quantity','param.price')
         ->join(self::param_table.' as param','p.id','=','param.package_id')
         ->where('p.id',$packageId)
         ->get();
      if($params)
      {
         $package->params = $params;
      }
      else
      {
         $package->params = [];
      }
      
      return $package;
   }

   //check package exists or not
   static function checkPackage(int $id):bool
   {
      $package = Package::where('id',$id)->first();
      return $package ? true : false;
   }




}
