<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;

class UserCtrl extends Controller 
{
	use ResponseTrait;
	use ResponseCodeTrait;

	/*
	get all users by status
	*/
	function getAllUsers(int $status=1)
	{
		$user = $this->getAll($status);
	}	
	
	/*
	get one user by status & id
	*/
	function userDetail(int $id,$status=1)
	{

		$user = $this->getOne($id,$status);

		return $user ? $this->response($this->ok,$user) : $this->response($this->failed,'User not found');	
	}

	function infoUpdate(Request $request)
	{
		//do some validation
		$validate = $this->validateProfile($request->all());

		if($validate->fails())
		{
			$this->response($this->vError,$validate->errors());
		} ;

		$authId = $request->user()->id;
		$post = $request->all();
		$post['id'] = $authId;
		
		return $this->update($post)? $this->response($this->ok,'Profile info updated'): $this->response($this->failed,'Profile info update failed');
	}

	function avatarUpdate(Request $request)
	{
		//validate avatar
		$validate = $this->avatarValidation($request->all());
		if($validate->fails())
		{
			return $this->response($this->vError,$validate->errors());
		}

		$file = $request->file('avatar_img');
		$fileName = $file->getClientOriginalName();
		$post = [
			'avatar_img'=> $fileName,
			'id'=> $request->user()->id
		];


		if($this->update($post))
		{
			//store file
			$store = $file->storeAs('avatars',$fileName);

			return $store ? $this->response($this->ok,'Avatar image updated') : $this->response($this->failed,'Avatar image store failed');
		}
		return $this->response($this->failed,'Avatar image update failed');		
	}
		
	function coverImgUpdate(Request $request)
	{
		$validate = $this->coverValidation($request->all());
		if($validate->fails())
		{
			return $this->response($this->vError,$validate->errors());
		}

		$file = $request->file('cover_img');
		$fileName = $file->getClientOriginalName();
		$post = [
			'cover_img'=> $fileName,
			'id'=> $request->user()->id
		];

		if($this->update($post))
		{
			$store = $file->storeAs('covers',$fileName);

			return $store ? $this->response($this->ok,'Cover image updated') : $this->response($this->failed,'Cover image store failed');
		}
		return $this->response($this->failed,'Cover image update failed');
	}
	
	function updatePassword(Request $request)
	{

		$validate = $this->passwordValidation($request->all());
		if($validate->fails())
		{
			return $this->response($this->vError,$validate->errors());
		}
		$auth = $request->user();
		//check pass
		$check = Hash::check($request->old_password,$auth->password);
		if($check)
		{
			//update 
			$post = [
				'password'=> bcrypt($request->password),
				'id'=> $auth->id
			];

			return $this->update($post) ? $this->response($this->ok,'Password updated'): $this->response($this->failed,'Password update failed');
		}

		return $this->response($this->failed,'Old password doest not matched.');
	}

    function logout(Request $request)
    {
    	//revoke token
    	$request->user()->token()->revoke();
    	return $this->response($this->ok,'Logout success!');
    }


    function getOne(int $id, int $status):object
    {
    	$avatar = url('/storage/app/avatars').'/';
    	$cover = url('/storage/app/covers').'/';
    	$user =  DB::table('users as u')
    		->join('roles as r','r.id','=','u.role_id')
    		->selectRaw('u.id,u.username,u.email,concat("'.$avatar.'",u.avatar_img) as avatar_img,concat("'.$cover.'",u.cover_img) as cover_img, date_format(u.created_at,"%b %e %Y") as created_at ,u.fname,u.lname,u.mobile,u.marital_status,u.date_of_birth,u.place_of_birth,u.gender,u.religion,u.nationality,u.nid,u.designation,r.name as role')
    		->where('u.id',$id)
    		->first();
    	return $user;
    }

    function getAll(int $status):array
    {
    	return User::where('user_status',$status)->paginate(20);
    }
    function create(array $post):bool
    {

    }

    function update(array $post):bool
    {
    	return User::where('id',$post['id'])->update($post);
    }

    function delete(int $id):bool
    {

    }

    function validateProfile(array $request)
    {
    	return Validator::make($request,[
    		'fname'=> 'required',
    		//'lname'=> 'required',
    		'mobile'=> 'required|numeric|digits_between:11,11',
    		'date_of_birth'=> 'required|date_format:Y-m-d',
    		'place_of_birth'=> 'required',
    		'gender'=> 'required',
    		'marital_status'=> 'required',
    		'nationality'=> 'required',
    		'nid'=> 'required|numeric|digits_between:13,20',
    		'religion'=> 'required',
    		'designation'=> 'required',
    	]);
    }
    function passwordValidation($request)
    {

		return Validator::make($request,[
            'old_password'=> 'required',
            'password'=> 'required|min:6|confirmed',
            
           
		]);    	
    }

    function avatarValidation($request)
    {
    	return Validator::make($request,[
    		'avatar_img'=> 'required| mimes:jpeg,png,jpg,gif|max:500'
    	]);
    }    

    function coverValidation($request)
    {
    	return Validator::make($request,[
    		'cover_img'=> 'required| mimes:jpeg,png,jpg,gif|max:3072'
    	]);
    }    
}
