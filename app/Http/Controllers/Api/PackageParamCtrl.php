<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\PackageParam;

class PackageParamCtrl extends Controller
{
	private static $table ="package_params";
	use ResponseTrait;
	use ResponseCodeTrait;

      //get parameters by package id
   	static function getParametersByPackId(int $packageId):object
   	{
         return PackageParam::where('package_id',$packageId)->get();
   	}

   	static function createMultiParam(array $post):bool
   	{
	   	//inserting multiple record once
	   	$insert =  DB::table(self::$table)->insert($post);
	   	//$insert =  PackageParam::create([$post]);
	   	return $insert ? true : false;
   	}

   	// static function update(array $post):bool
   	// {
   	// 	return PackageParam::where('id',$post['id'])->update($post);
   	// }

   	// static function delete(int $id):bool
   	// {
   	// 	return PackageParam::destroy($id);
   	// }


}
