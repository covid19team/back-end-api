<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\PackageParamCtrl as Param;
use App\Http\Controllers\Api\PackageCtrl as Package;
use App\Http\Controllers\Api\InstituteCtrl as Institute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PackageParamHandleCtrl extends Controller implements CrudInterface
{
   use ResponseTrait;
   use ResponseCodeTrait;
   use CrudModel;
   const package_table = "packages";
   const param_table = "package_params";   

   const package_path = "App\Package";
   const param_path = "App\PackageParam";

   function allPackages()
   {

      $packages = $this->getAll(self::package_path);

      return count($packages)>0 ? $this->response($this->ok,$packages) : $this->response($this->ok,[]); 
   }

   //require packge id return package detail with param list
   function packageDetail($packageId)
   {
      $check = Package::checkPackage($packageId);
      if($check)
      {
        $package = Package::singlePackageDetail($packageId);

        return $package ? $this->response($this->ok,$package) : $this->response($this->ok,$package);        
      }

      return $this->response($this->failed,"Invalid package id");
   }

   /**
    * this function is intended for creating 
    * package,parameters,institute
    * using transaction and rollback
    */
   function savePackage(Request $request)
   {
         $packageInfo = [
            'display'=>$request->package['display'],
            'title'=> $request->package['title'],
            'description'=> $request->package['description'],
        ];
      print_r($packageInfo);exit;
      //validate package
      $validatePack = $this->packageValidation($packageInfo);
      if($validatePack->fails())
      {
         return $this->response($this->vError,$validatePack->errors());
      }     

      $paramInfo = $request->parameters;
        //validate each parameter and 
        foreach($paramInfo as $key=>$a)
        {
            //init package id in paramter
            //$paramInfo[$key]['package_id'] = $packageId;

            $validate = $this->paramValidation($paramInfo[$key]);
            if($validate->fails())
            {
               
               return $this->response($this->vError,$validate->errors());
            }       
        }  

        DB::beginTransaction();
         //save package
         $package = $this->create($packageInfo,self::package_path);
        if($package)
        {
            $packageId = $package->id;

            //save parameters
            $paramInfo = $request->parameters;
            //validate each parameter
            foreach($paramInfo as $key=>$a)
            {
               //init package id in paramter
               $paramInfo[$key]['package_id'] = $packageId;
       
            }     
            //save parameter direct from param class not from crud model. it is for inserting multiple record

            $save = Param::createMultiParam($paramInfo);
            if($save)
            {
               DB::commit();
               $res = ['package_id'=>$packageId];
               return $this->response($this->ok,$res);   
            }

            DB::rollBack();
            return $this->response($this->failed,'Parameter creation failed');
         
      }
      return $this->response($this->failed,'Package creation failed'); 

   }

      function updatePackage(Request $request)
      {
 
         //if id not set return
         if(!isset($request->id) OR $request->id <1)
         {
          return $this->response($this->failed,'Package id missing');
         }

         $validate = $this->packageValidation($request->all());
         if($validate->fails())
         {
            return $this->response($this->vError,$validate->errors());
         }  

         return $this->update($request->all(),self::package_path) ? $this->response($this->ok,'Package updated successfully') :  $this->response($this->failed,'Package update failed');
      }

      function deletePackage(int $id)
      {

         return $this->delete($id,self::package_path) ? $this->response($this->ok,'Package deleted successfully') :  $this->response($this->failed,'Package deletion failed');
      }

      
      //param handle

      function getParameterByPackageId(int $packageId)
      {
         $parameters = Param::getParametersByPackId($packageId);
         return count($parameters)>0 ? $this->response($this->ok,$parameters) : $this->response($this->ok,[]);
      } 
      
      function saveParameter(Request $request)
      {
        
         $paramInfo = $request->all();
         
         foreach($paramInfo as $key=>$a)
           {
               //init package id in paramter
               //$paramInfo[$key]['package_id'] = $packageId;

               $validate = $this->paramValidation($paramInfo[$key]);
               if($validate->fails())
               {
                  
                  return $this->response($this->vError,$validate->errors());
               }  
               if(!array_key_exists('package_id', $paramInfo[$key]) || $paramInfo[$key]['package_id']=='') 
               {
                   return $this->response($this->vError,['package_id'=>"package_id missing"]);
               }    
           } 
           
           return Param::createMultiParam($paramInfo) ? $this->response($this->ok,"Parameter saved successfully") :  $this->response($this->failed,"Parameter save failed");

      }

      function updateParameter(Request $request)
      {

         //if id not set return
         if(!isset($request->id) OR $request->id <1)
         {
            return $this->response($this->failed,'Param id missing');
         }

         $validate = $this->paramValidation($request->all());
         if($validate->fails())
         {
            return $this->response($this->vError,$validate->errors());
         }      
         return $this->update($request->all(),self::param_path) ? $this->response($this->ok,'Package parameter updated successfully') :  $this->response($this->failed,'Package parameter update failed');
      }

      function deleteParameter(int $id)
      {
         return $this->delete($id,self::param_path) ? $this->response($this->ok,'Package parameter deleted successfully') :  $this->response($this->failed,'Package parameter deletion failed');
      }

      function packageValidation($request)
      {
         return Validator::make($request,[
         'title'=> 'required|string|max:255',
         'description'=>'required'
         ]);
      }

      function paramValidation($request)
      {

         return Validator::make($request,[
          
           "name"=> "required|string|max:255 ",
           "quantity"=> 'required|numeric',
           "price"=> 'required|numeric'
         ]);
      }
      
   
}
