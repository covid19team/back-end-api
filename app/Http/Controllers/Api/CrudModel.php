<?php
namespace App\Http\Controllers\Api;

trait CrudModel
{
	/*
		*this trait is intendent for providing simple 
		crud operation
		*its not intendent for using any joining
		*its not intendent for using where clause
		*getAll,create(signle),update(single),delete(single)
		*just for using above simple operation and reduce code inspired from DRY 
	*/

	function getAll(string $table): object
	{
		return $table::get();
	}

	function create(array $post,string $table):object
	{
		
		return $table::create($post);
	}	


	function update(array $post,string $table):bool
	{
		return $table::where('id',$post['id'])
			->update($post);
	}

	function delete(int $id,string $table):bool
	{
		return $table::destroy($id);
	}
}
?>