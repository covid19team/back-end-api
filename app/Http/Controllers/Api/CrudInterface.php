<?php 

namespace App\Http\Controllers\Api;

interface CrudInterface{

    function getAll(string $table): object;

    function create(array $post,string $table):object;

    function update(array $post,string $table):bool;
    
    function delete(int $id,string $table):bool;
}

?>