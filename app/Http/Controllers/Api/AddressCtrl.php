<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Address;

class AddressCtrl extends Controller
{
	use ResponseTrait;
	use ResponseCodeTrait;

    function userAddress(int $userId)
    {
    	$address = $this->get($userId);

    	return $address ? $this->response($this->ok,$address) : $this->response($this->failed,'Address not found'); 
    }

    function updateUserAddress(Request $request)
    {

    	$update = $this->insertOrUpdate($request->all());
    
    	return $update ? $this->response($this->ok,'Address updated') : $this->response($this->failed,'Address update failed');
    }

    function get(int $userId)
    {
    	return Address::firstWhere('user_id',$userId);
    }

    function insertOrUpdate(array $post)
    {
    	$find = $this->get($post['user_id']);
    	if($find)
    	{
    		//update 
    		return Address::where('user_id',$post['user_id'])->update($post);
    	}
    	else
    	{
    		$add = Address::create($post);
    		return $add->save();
    	}
    }
}
