<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageParam extends Model
{
    protected $table = "package_params";
    protected $guarded = [];
    public $timestamps = false;
}
