import Vue from 'vue';
import VueRouter from 'vue-router';
import LoginComponent from './components/LoginComponent';
import DashboardComponent from './components/DashboardComponent';

Vue.use(VueRouter);

//routes 
const routes = [
	{
		path:'/',
		redirect: '/login'
	},
	{
		path: '/login',
		component: LoginComponent,
		name: 'login'
	},
	{
		path:'/dashboard',
		component: DashboardComponent,
		name: 'dashboard',
		beforeEnter: (to, from, next) => {
			let token = localStorage.getItem('token');
			if(token )
			{
				next();
			}
			else
			{
				next('/login');
			}
		},
		
	}
];

export default new VueRouter({routes});