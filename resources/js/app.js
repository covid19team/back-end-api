require('./bootstrap');
window.Vue = require('vue');

import router from './router';
import vuetify from './vuetify';

//import components
import AppComponent from './components/AppComponent';
//import components end


//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

new Vue({
    el: '#app',
    router,
    vuetify,
    components: {
    	'app-component': AppComponent,
    },
});
